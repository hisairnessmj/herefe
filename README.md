<p align="center">
    <h3 align="center">hereFE </h3>
      <p align="center">
        Automation testing for wegodeliver web-page
      </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#built-with">Built With</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation-and-running-all-tests">Installation and running all tests</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a>
       <ul>
        <li><a href="#project-structure">Project structure</a></li>
        <li><a href="#running-tests-one-by-one">Running tests one by one</a></li>
        <li><a href="#running-tests-from-testng-file">Running tests from testng file</a></li>
      </ul>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->

### Built With

* [selenium]()
* [selenide]()
* [testNG]()
* [pdfbox]()
* [opencsv]()
* [log4j]()
* [allure]()
* [lombok]()

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* maven
* java 1.8

### Installation and Running all tests

1. Clone the repo
   ```sh
   git clone https://gitlab.com/hisairnessmj/hereproject.git
   ```

2. Open it as a maven project in your IDE


3. Run command in project directory
   ```sh
   mvn clean test
   ```

<!-- USAGE EXAMPLES -->

## Usage

You can run project with `mvn clean test` command, it should build project and run tests inside. All tests are listed in
the file `testng.xml`, which is located in root directory. This file is pointed for the maven surefire plugin as suite
Xml file.

When test are started, browser will be opened and closed for every test automatically.
Chrome browser portable is included in project to avoid discrepancies between version of chromedriver and actual browser
version. Thus, project is autonomous.

During tests all logs will be in console and report will be generated.
Under test output files will be created in the root of project and renamed further to avoid collisions.

After test completion, you will have allure report. Run `mvn allure:serve` command to generate report and open it in 
your browser. Maven should download all necessary dependencies and create folder `.allure`
in the root of project. After that, report should open automatically.

- In case it was not, allure report can be found in temp directory in html format:

  ```sh
  C:\Users\{username}\AppData\Local\Temp\16006777742405910605\allure-report
  ```
In Allure Report you can see all tests with detailed steps.

Run `mvn allure:serve` every time you need to refresh test results after changes.

All tests and steps can be found in folder:

  ```sh
  src/test/java/com/here/selenide/tests
  ```

### Project structure


In the package `src.main.java.com.here.selenide` the core logic located.
Package `elements` create custom Page elements, package `utils` has classes to work
with external files. Package `webpages` has all pages as Page Objects with methods and web elements. One of pages is
`Input` which contains custom Page Elements. In package `webdriver` here is class to prepare and launch browser driver.

In the folder `src/main/driver` here is chromedriver for Chrome browser.
Please be aware, that all tests checked in Chrome browser only.

In the package `com.here.selenide.tests` you can find 5 tests as TestNg tests. Test Data should be added here, or
in `template.csv` file. Tests checked adding different addresses, creating routes and output files data.

In the folder `src/test/resources` here is csv template with test data for uploading on site.

### Running tests one by one

Every test can be run in IDE from its class as TestNG test. Thus, one by one run provide more details.

### Running tests from testng file

All tests can be run from `testng.xml` file as well.