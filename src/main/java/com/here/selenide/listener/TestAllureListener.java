package com.here.selenide.listener;

import com.here.selenide.webdriver.Browser;
import io.qameta.allure.Attachment;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

@Log4j2
public class TestAllureListener implements ITestListener {

    public static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    //HTML attachment
    @Attachment(value = "{0}", type = "text/html")
    public static String attachHTML(String html) {
        return html;
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        log.info(getTestMethodName(iTestResult) + " failed");
        WebDriver driver = Browser.getDriver();
        saveScreenshotPNG(driver);
        attachHTML(driver.getPageSource());
    }
}
