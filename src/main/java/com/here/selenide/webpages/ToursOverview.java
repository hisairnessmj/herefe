package com.here.selenide.webpages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.Getter;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Getter
public class ToursOverview extends GreetingFleetAndDepot {

    private String pageName = "Tours overview";

    private SelenideElement tourDetails = $x("//h1[text() = 'Tour details']");

    private SelenideElement firstPoint = $x("//div[@title='Depot']/following::div[text()='1' and not((@style))]");

    private SelenideElement firstAddress = $x("//div[@title='Depot']/following::div[text()='1']/following::div[@title][1]");

    private SelenideElement secondPoint = $x("//div[@title='Depot']/following::div[text()='2' and not((@style))]");

    private SelenideElement secondAddress = $x("//div[@title='Depot']/following::div[text()='2']/following::div[@title][1]");

    private SelenideElement thirdPoint = $x("//div[@title='Depot']/following::div[text()='3' and not((@style))]");

    private SelenideElement thirdAddress = $x("//div[@title='Depot']/following::div[text()='3']/following::div[@title][1]");

    private SelenideElement downloadPdf = $x("//div[@id='button-tour-details-pdf']");

    private SelenideElement downloadCsv = $x("//div[@id='button-tour-details-csv']");

    private ElementsCollection getDirections = $$x("//a[@title='Get directions']");

    private ElementsCollection shareButtons = $$x("//div[contains(@id, 'button-tour')]");

    private ElementsCollection tripData = $$x("//div/p/following-sibling::strong");

    private ElementsCollection pointsOnMap = $$x("//div[@title='Depot']/following::div[text() and ((@style))]");

    private SelenideElement noPossiblePlanning = $x("//strong[contains(text(),'It was not possible to plan')]");

    private SelenideElement previous = $x("//div[@id='button-solution-back']");

    private SelenideElement assignedOrders = $x("//p[text()='Assigned orders']/following::strong[1]");

    private SelenideElement vehiclesUsed = $x("//p[text()='vehicles used']/following::strong[1]");

    private SelenideElement downloadCsvUnussigned = $x("//div[@id='button-download-unassigned']");

    private ElementsCollection buttonViewTour = $$x("//div[contains(@id, 'button-view-tour')]");

    private SelenideElement buttonNextTour = $x("//strong[text() = 'Next tour']/parent::div");

    private SelenideElement buttonDownloadAssigned = $x("//div[@id='button-download-assigned']");

    @Step("Checked address on page with number {0}")
    public ToursOverview checkAdress(String numberOfPoint, String adress) {
        String xpath = String.format("//div[@title='Depot']/following::div[text()='%s']/following::div[@title][1]",
                numberOfPoint);
        SelenideElement currAdress = $x(xpath);
        currAdress.shouldHave(exactText(adress))
                .shouldHave(attribute("title", adress));
        return this;
    }

    @Step("Checked buttons on page with number")
    public ToursOverview checkButtons() {
        shareButtons.stream().collect(Collectors.toList()).forEach(e -> {
            e.shouldBe(visible);
            e.shouldBe(enabled);
        });
        return this;
    }

    @Step("Checked get direction button")
    public ToursOverview checkGetDirections() {
        new ArrayList<>(getDirections).forEach(e -> {
            e.shouldBe(visible);
            e.shouldBe(enabled);
            e.getAttribute("href").substring(0, 60)
                    .matches("(https:'/'/share.here.com'/r'/\\d+.\\d+,\\d+.\\d+\\/\\d+.\\d+,\\d+.\\d+'?)");
        });
        return this;
    }

    @Step("Checked get direction button")
    public ToursOverview checkTripData(String stops, String orders) {
        tripData.stream().forEach(e -> e.shouldBe(visible));
        tripData.get(0).shouldHave(exactText(stops));
        tripData.get(1).shouldHave(ownText(orders));
        tripData.get(2).shouldHave(matchText("\\d+"));
        return this;
    }

    @Step("Checked available point on the map")
    public ToursOverview checkMapPoints() {
        new ArrayList<>(pointsOnMap).forEach(e -> {
            e.shouldBe(visible);
            e.shouldBe(enabled);
            e.shouldHave(matchText("(\\d+)"));
        });
        return this;
    }

    @Step("Checked string data in Excel")
    public ToursOverview checkDataInExcel(String checkedStr, String... arr) {
        List<String> list = Arrays.asList(arr);
        Assert.assertTrue(list.contains(checkedStr));
        return this;
    }

    @Step("Click on element")
    public ToursOverview click(SelenideElement element) {
        super.click(element);
        return this;
    }

    @Step("Scrolled to element")
    public ToursOverview scrollToEl(SelenideElement el) throws InterruptedException {
        super.scrollToEl(el);
        return this;
    }
}
