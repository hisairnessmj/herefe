package com.here.selenide.webpages;

import com.codeborne.selenide.WebDriverRunner;
import com.here.selenide.elements.ExtendedFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractPage {

    private String pageName;
    public void init() {
        PageFactory.initElements(new ExtendedFieldDecorator(WebDriverRunner.getWebDriver()), this);
    }

    public String getPageName() {
        return pageName;
    }
}
