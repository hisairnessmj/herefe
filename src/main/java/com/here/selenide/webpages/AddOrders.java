package com.here.selenide.webpages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.conditions.Attribute;
import com.here.selenide.elements.TextField;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.here.selenide.entity.User;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Getter
public class AddOrders extends GreetingFleetAndDepot {

    private String pageName = "Setting Orders";

    private SelenideElement addOrdersHeader = $x("//div/h1[contains(text(),'Add orders')]");

    private SelenideElement manualTab = $x("//span[text()='Manual']/parent::div");

    private SelenideElement importTab = $x("//div[@id='button-orders-mode-Import']");

    private SelenideElement addFile = $x("//div[@id='react-file-drop-demo']/preceding::input");

    @FindBy(xpath = "//div[@class='css-mudjgk']")
    private ElementsCollection columnsInput;

    private SelenideElement columnsInputNameId = $x("//div[@class][contains(text(), 'Name/ID')]");
    private SelenideElement columnsInputAddress = $x("//div[@class][contains(text(), 'Address')]");
    private SelenideElement columnsInputDemand = $x("//div[@class][contains(text(), 'Demand')]");

    @FindBy(xpath = "//div[@alt='ID']/parent::div//select/option")
    private ElementsCollection categorySelect;

    @FindBy(xpath = "//strong[text()='Plan tour']/parent::div")
    private SelenideElement saveInput;

    @FindBy(xpath = "//div[@title = 'Address']/parent::div/..//div[@title][@alt]")
    private ElementsCollection categoriesInput;

    @FindBy(xpath = "//div[@title = 'Address']/parent::div/..//div[@title and not(@Alt)]")
    private ElementsCollection valuesInput;

    private SelenideElement confirmInput = $x("//strong[text()='Confirm']/parent::div");

    private SelenideElement custPhoneInput = $x("//input[@id='input-edit-order-phone']");

    private SelenideElement plus = $x("//div[@id='button-add-order-manual']");

    private ElementsCollection custAddressVariants = $$x("//div[@id='input-edit-order-add-options']/div");

    private SelenideElement sideMenu = $x("//button[@type='button'][@class]");

    private SelenideElement gridWithCust = $x("//div[contains(@id, 'label-order-name-0')]/parent::div");

    private ElementsCollection customerDetails = $$x("//div[@title='Name/ID']/parent::div/..//div[@id]//div[@title]");

    private SelenideElement planTour = $x("//strong[contains (text(), 'Plan tour')]");

    private SelenideElement noSuchAddress = $x("//p[contains(text(),'We could not find this address')]");

    private SelenideElement noOrdersEntered = $x("//p[contains(text(),'We could not find this address')]");

    private SelenideElement removeOrders = $x("//div[@id='link-remove-orders']");

    @Step("Added customer with correct data and name {2}")
    public void fillCustomer(TextField nameF, TextField addressF, User user) {
        printByChar(nameF, user.getName());  //
        printByChar(addressF, user.getAddress());  //
        custAddressVariants
                .shouldBe(CollectionCondition.allMatch("d", WebElement::isDisplayed));
        custAddressVariants.findBy(text(user.getFullAddress())).click();   //
        //custAddressInput.sendKeys(Keys.ENTER);
        custPhoneInput.sendKeys(user.getPhone());
        click(plus);
    }

    @Step("Customer with corrupted data has been put")
    public void fillCustomerNegative(TextField nameF, TextField addressF, String name, String address) {
        printByChar(nameF, name);  //
        printByChar(addressF, address);
        custPhoneInput.pressEnter();
    }

    @Step("Customer {0} with correct data checked")
    public void checkCustomer(User user) {
        customerDetails
                .shouldBe(CollectionCondition.allMatch("d", WebElement::isDisplayed))
                .findBy(Attribute.attribute("title", user.getName())) //
                .shouldHave(exactText(user.getName())); //
        customerDetails.findBy(text(user.getAddress()))
                .shouldHave(Condition.attribute("title", user.getFullAddress()));
    }

    @Step("Checked columns on page after file uploading")
    public void checkColumns(String... arr) {
        List<String> excelStrings = Arrays.asList(arr);
        List<String> listOnPage = new ArrayList<>();
        listOnPage.add(columnsInputAddress.text());
        listOnPage.add(columnsInputDemand.text());
        for (String val : listOnPage) {
            Assert.assertTrue(excelStrings.contains(val.toLowerCase()));
        }
    }

    @Step("Checked categories options on page after file uploading")
    public void checkCategoryOptions(String[] arr) {
        List<String> list = Arrays.asList(arr);
        Assert.assertTrue(categorySelect.texts().containsAll(list));
    }

    @Step("Checked categories on page before confirmation")
    public void checkCategories() {
        String[] arr = {"ID", "Name", "Phone", "e-mail", "Address", "Notes",
                "Start Time", "End Time", "Service Time (min)", "Demand"};
        List<String> list = categoriesInput.texts();
        for (String val : arr) {
            Assert.assertTrue(list.contains(val));
        }
    }

    @Step("Checked input values on page before confirmation")
    public void checkInputValues(String id,
                                 String name,
                                 String phone,
                                 String email,
                                 String address,
                                 String orderNotes,
                                 String startTime,
                                 String endTime,
                                 String serviceTime,
                                 String demand) {
        String[] arr = {id, name, phone, email, address, orderNotes, startTime, endTime, serviceTime, demand};
        List<String> list = valuesInput.texts();
        for (String val : arr) {
            Assert.assertTrue(list.contains(val));
        }
    }
    @Step("Click on element")
    public AddOrders click(SelenideElement element) {
        super.click(element);
        return this;
    }

    @Step("Scrolled to element")
    public AddOrders scrollToEl(SelenideElement el) throws InterruptedException {
        super.scrollToEl(el);
        return this;
    }
}
