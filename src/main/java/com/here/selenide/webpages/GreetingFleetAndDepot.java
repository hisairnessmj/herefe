package com.here.selenide.webpages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.here.selenide.elements.TextField;
import io.qameta.allure.Step;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Getter
@Log4j2
public class GreetingFleetAndDepot extends AbstractPage {

    private String pageName = "Greetings and Fleet and Depot";

    public SelenideElement nextButton = $x("//div[(text() ='Skip')]/following-sibling::div[@class]");

    private SelenideElement startPlanningButton = $x("//strong[contains(text(),'Start planning')]" +
            "/parent::div");

    private SelenideElement accCookiesBut = $x("//strong[contains(text(),'Accept cookies')]");

    private SelenideElement fleetAndDepot = $x("//div[contains(text(),'Fleet & Depot')]/parent::div");

    private SelenideElement companyName = $x("//h1[text()='Company name']");

    private ElementsCollection addressVariants = $$x("//div[@id='input-depot-loca-options']/div");

    @FindBy(xpath = "//strong[contains (text(), 'Next')]")
    private SelenideElement nextToCustomerButton;

    private SelenideElement selectMode = $x("//select/option[@value='solo']//parent::select");

    @Step("Printed chars: {1}")
    public GreetingFleetAndDepot printByChar(TextField input, String str) {
        char[] arr = str.toCharArray();
        for (char c: arr) {
            input.sendKeys(String.valueOf(c));
        }
        return this;
    }

    @Step("Scrolled to element")
    public GreetingFleetAndDepot scrollToEl(SelenideElement el) throws InterruptedException {
        Selenide.executeJavaScript("arguments[0].scrollIntoView(true);", el);
        Thread.sleep(500);
        return this;
    }

    @Step("Click on element")
    public GreetingFleetAndDepot click(SelenideElement element) {
        //log.info(String.format("Click on element: " + element.getTagName()));
        element.click();
        return this;
    }
}
