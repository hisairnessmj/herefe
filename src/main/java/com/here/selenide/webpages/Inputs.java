package com.here.selenide.webpages;

import com.codeborne.selenide.WebDriverRunner;
import com.here.selenide.elements.ExtendedFieldDecorator;
import com.here.selenide.elements.TextField;
import com.here.selenide.elements.impl.TextFieldImpl;
import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class Inputs {

    public void init() {
        PageFactory.initElements(new ExtendedFieldDecorator(WebDriverRunner.getWebDriver()), this);
    }

    @FindBy(xpath = "//h1[text()='Company name']/following-sibling::input")
    private TextField companyNameInput;

    @FindBy(xpath = "//input[@placeholder='Type address here, including the city...']")
    private TextField companyAddressInput;

    @FindBy(xpath = "//input[@id='input-fleet-amount']")
    private TextField numberOfVehicles;

    @FindBy(xpath = "//input[@id='input-fleet-capacity']")
    private TextField capacityPerVehicle;

    @FindBy(xpath = "//input[@id='input-edit-order-name']")
    private TextField custNameInput;

    @FindBy(xpath = "//input[contains (@id, 'input-edit-order-add')]")
    private TextField custAddressInput;
}
