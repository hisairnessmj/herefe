package com.here.selenide.utils;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class ExcelReader {

    public static List<String[]> readAsStrings(String fileName, int skipLine, char separator) throws IOException, URISyntaxException, CsvException {
        CSVParser csvParser = new CSVParserBuilder().withSeparator(separator).build();  // custom separator
        List<String[]> r;

        try(CSVReader reader = new CSVReaderBuilder(
                new FileReader(fileName))
                .withCSVParser(csvParser)   // custom CSV parser
                .withSkipLines(skipLine)    // skip the first line, header info
                .build()){
            r = reader.readAll();
        }
        return r;
    }
}
