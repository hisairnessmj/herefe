package com.here.selenide.utils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class PDFReader {

    public static String getPdfContent(String path) {
        String content = null;
        try {
             content = readPdfContent(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public static String readPdfContent(String path) throws IOException {

        FileInputStream fis = new FileInputStream(path);
        BufferedInputStream bf = new BufferedInputStream(fis);
        PDDocument doc = PDDocument.load(bf);
        PDFTextStripper pdfStrip = new PDFTextStripper();
        String content = pdfStrip.getText(doc);
        doc.close();

        return content;
    }

    public static int getPageCount(PDDocument doc) {
        int pageCount = doc.getNumberOfPages();
        return pageCount;
    }
}