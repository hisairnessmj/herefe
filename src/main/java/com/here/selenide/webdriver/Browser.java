package com.here.selenide.webdriver;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import net.lightbody.bmp.BrowserMobProxyServer;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.html5.LocationContext;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

@Log4j2
public class Browser {

    private static WebDriver webDriver;

    private static BrowserMobProxyServer proxyServer;

    public static WebDriver getDriver() {
        if (webDriver == null) {
            loadDriver();
        }
        return webDriver;
    }

    public static BrowserMobProxyServer getProxyServer() {
        return proxyServer;
    }

    private static void setDriver(WebDriver wDriver) {
        webDriver = wDriver;
    }

    @SneakyThrows
    public static void loadDriver() {
        HashMap<String, Object> chromePrefs = new HashMap<>();

        proxyServer = new BrowserMobProxyServer();
        proxyServer.start(8888);
        final Proxy proxyConfig = new Proxy().setHttpProxy("127.0.0.1:8888")
                .setSslProxy("127.0.0.1:8888");

        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", System.getProperty("user.dir"));
        chromePrefs.put("profile.default_content_setting_values.geolocation", 1);

        ChromeOptions options = new ChromeOptions().setProxy(proxyConfig);
        options.setAcceptInsecureCerts(true)
                .addArguments("--lang=en-us")
                .addArguments("--start-maximized")
                .setExperimentalOption("prefs", chromePrefs);

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        ((LocationContext) driver).setLocation(new Location(52.520008, 13.404954, 0));
        setDriver(driver);
        WebDriverRunner.setWebDriver(driver);

        proxyServer.newHar("https://wegodeliver.here.com/");
    }

    @SneakyThrows
    public static void loadRemoteDriver() {
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", "C:\\Users\\ruabrdp\\Downloads");
        chromePrefs.put("profile.default_content_setting_values.geolocation", 1);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=en-us");
        options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", chromePrefs);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setBrowserName("chrome");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        RemoteWebDriver webDriver = new RemoteWebDriver(new URL("http://172.16.20.199:4444/wd/hub"), capabilities);
        ((LocationContext) webDriver).setLocation(new Location(52.520008, 13.404954, 0));
        setDriver(webDriver);
        WebDriverRunner.setWebDriver(webDriver);
    }

    public static String checkDownloadedPdf() {
        File folder = new File(System.getProperty("user.dir"));
        File[] listOfFiles = folder.listFiles();
        boolean found = false;
        boolean renamed = false;

        File newPdfFileName = new File(System.getProperty("user.dir")
                + File.separator
                + String.format("Tour 1 - WeGo Deliver 2021-09 #%s.pdf",
                ThreadLocalRandom.current().nextInt(1, 1000 + 1)));

        File resultFile = null;

        for (File listOfFile : listOfFiles) {  //find file
            if (listOfFile.isFile()) {
                String fileName = listOfFile.getName();
                if (fileName.matches("\\w+\\.tmp") ||
                        fileName.matches("\\w+ \\d+ - \\w+ \\w+ \\d+-\\d+-\\d+\\.\\w+")) {
                    resultFile = new File(fileName);
                    found = true;
                    renamed = resultFile.renameTo(newPdfFileName);
                }
            }
        }

        Assert.assertTrue(found, "Downloaded file is not found");
        Assert.assertTrue(renamed, "Downloaded file is not renamed");

        return newPdfFileName.toPath().toString();
    }

    public static String checkDownloadedPdfRemotely() {
        File folder = new File("C:\\Users\\ruabrdp\\Downloads");
        File[] listOfFiles = folder.listFiles();
        boolean found = false;
        boolean renamed = false;

        File newPdfFileName = new File("C:\\Users\\ruabrdp\\Downloads"
                + File.separator
                + String.format("Tour 1 - WeGo Deliver 2021-09 #%s.pdf",
                ThreadLocalRandom.current().nextInt(1, 1000 + 1)));

        File resultFile = null;

        for (File file : listOfFiles) {  //find file
            if (file.isFile()) {
                String fileName = file.getName();
                if (fileName.matches("\\w+\\.tmp") ||
                        fileName.matches("\\w+ \\d+ - \\w+ \\w+ \\d+-\\d+-\\d+\\.\\w+")) {
                    resultFile = new File(fileName);
                    found = true;
                    renamed = resultFile.renameTo(newPdfFileName);
                }
            }
        }

        Assert.assertTrue(found, "Downloaded file is not found");
        Assert.assertTrue(renamed, "Downloaded file is not renamed");

        return newPdfFileName.toPath().toString();
    }

    public static String checkDownloadedCsv() {
        File folder = new File(System.getProperty("user.dir"));
        File[] listOfFiles = folder.listFiles();
        boolean found = false;
        boolean renamed = false;

        File resultFile = null;

        File newCsvFile = new File(System.getProperty("user.dir")
                + File.separator
                + String.format("Tour 1 - file #%s.csv",
                ThreadLocalRandom.current().nextInt(1, 1000 + 1)));

        for (File listOfFile : listOfFiles) {  //find file
            if (listOfFile.isFile()) {
                String fileName = listOfFile.getName();
                if (fileName.matches("(Tour \\d+ - \\w+\\.csv)") ||
                        fileName.matches("Unassigned Orders \\d+-\\d+-\\d+\\.\\.\\w+") ||
                        fileName.matches("Tour Plan - WeGo Deliver \\d+-\\d+-\\d+\\.\\w+")) {
                    resultFile = new File(fileName);
                    found = true;
                }
            }
        }

        try {
            Files.move(resultFile.toPath(), newCsvFile.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            renamed = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assert.assertTrue(found, "Downloaded file is not found");
        Assert.assertTrue(renamed, "Downloaded file is not renamed");

        log.info(String.format("Excel file with name %s was found", newCsvFile.getName()));
        return newCsvFile.toPath().toString();
    }

    public static void cleanDownloads() {
        File folder = new File(System.getProperty("user.dir"));

        File[] listOfFiles = folder.listFiles();
        boolean flag = false;

        for (File file : listOfFiles) {
            if (file.isFile()) {
                String fileName = file.getName();
                if (fileName.matches("\\w+\\.pdf") || fileName.matches("\\w+\\.tmp") ||
                        fileName.matches("\\w+ \\d+ - \\w+ \\w+ \\d+-\\d+-\\d+\\.\\w+") ||
                        fileName.matches("\\w+ \\d+ - \\w+ \\w+ \\d+-\\d+\\s#\\d+\\.\\w+")) {
                    flag = file.delete();
                    if (!flag) log.warn(String.format("Downloaded file with name %s is not deleted", fileName));
                }
            }
        }
    }


    public static void closeBrowser() {
        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
        if (webDriver != null) {
            getDriver().quit();
            setDriver(null);
        }
        proxyServer.abort();
    }
}