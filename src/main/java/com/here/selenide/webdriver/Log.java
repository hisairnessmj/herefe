package com.here.selenide.webdriver;

import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.EventFiringWebDriver;

@Log4j2
public class Log {

    public static void decorateClick () {
        new EventFiringWebDriver(Browser.getDriver()).register(new AbstractWebDriverEventListener() {
            @Override
            public void afterClickOn(WebElement element, WebDriver driver) {
                log.info(String.format("Click on element: " + element.getTagName()));
            }
        });
    }

}
