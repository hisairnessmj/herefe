package com.here.selenide.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class User {
    String name;
    String address;
    String phone;
    String fullAddress;

    public User(String name, String address, String phone, String fullAddress) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.fullAddress = fullAddress;
    }
}
