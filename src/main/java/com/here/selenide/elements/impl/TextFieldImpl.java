package com.here.selenide.elements.impl;

import com.here.selenide.elements.TextField;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class TextFieldImpl extends AbstractElement implements TextField {
    protected TextFieldImpl(final WebElement wrappedElement) {
        super(wrappedElement);
    }

    @Override
    public void type(final String text) {
        wrappedElement.sendKeys(text);
    }

    @Override
    public void clear() {
        wrappedElement.sendKeys(Keys.CONTROL + "A");
        wrappedElement.sendKeys(Keys.BACK_SPACE);
        wrappedElement.sendKeys(Keys.ENTER);
    }

    @Override
    public void clearAndType(final String text) {
        clear();
        type(text);
    }

    @Override
    public void sendKeys(CharSequence ... chr) {
        wrappedElement.sendKeys(chr);
    }

    @Override
    public String getValue() {
        return wrappedElement.getAttribute("value");
    }

    @Override
    public void click() {
        wrappedElement.click();
    }
}
