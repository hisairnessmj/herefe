package com.here.selenide.elements;

public interface TextField extends Element {
    void type(String text);

    void clear();

    void clearAndType(String text);

    void sendKeys(CharSequence... enter);

    String getValue();

    void click();
}
