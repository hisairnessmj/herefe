package com.here.selenide.elements;

public interface Element {
    boolean isDisplayed();
}
