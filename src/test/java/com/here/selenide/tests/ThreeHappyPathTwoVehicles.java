package com.here.selenide.tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.here.selenide.listener.TestAllureListener;
import com.here.selenide.tests.steps.BaseSteps;
import com.here.selenide.utils.ExcelReader;
import com.here.selenide.webdriver.Browser;
import com.here.selenide.webpages.AddOrders;
import com.here.selenide.webpages.GreetingFleetAndDepot;
import com.here.selenide.webpages.Inputs;
import com.here.selenide.webpages.ToursOverview;
import com.opencsv.exceptions.CsvException;
import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;

@Log4j2
@Listeners({TestAllureListener.class})
public class ThreeHappyPathTwoVehicles extends Hooks {

    @Test
    public void threeHappyPathTwoVehicles() throws InterruptedException, IOException, URISyntaxException, CsvException {
        GreetingFleetAndDepot greetAndDepot = Selenide.page(GreetingFleetAndDepot.class);
        Inputs inputs = new Inputs();
        inputs.init();
        log.info(String.format("Opened page: %s", Selenide.title()));

        BaseSteps.skipIntro(greetAndDepot);
        log.info("Skipped introduction");

        greetAndDepot.getFleetAndDepot().shouldBe(Condition.visible);
        greetAndDepot.click(greetAndDepot.getCompanyName());
        inputs.getCompanyNameInput().sendKeys("someCompany");
        greetAndDepot.printByChar(inputs.getCompanyAddressInput(), "Nalepastraße 1, 12459 Berlin");

        greetAndDepot.getAddressVariants().findBy(text("Nalepastraße 1, 12459 Berlin, Germany")).click();

        Assert.assertEquals(inputs.getCompanyAddressInput().getValue(), "Nalepastraße 1, 12459 Berlin, Germany");
        greetAndDepot.scrollToEl(greetAndDepot.getSelectMode());
        greetAndDepot.getSelectMode().selectOptionByValue("custom");
        inputs.getNumberOfVehicles().clear();
        greetAndDepot.printByChar(inputs.getNumberOfVehicles(), "2");
        inputs.getCapacityPerVehicle().clear();
        greetAndDepot.printByChar(inputs.getCapacityPerVehicle(), "2");

        log.info("Multi vehicle mode has chosen");

        greetAndDepot.click(greetAndDepot.getNextToCustomerButton());

        AddOrders addOrders = Selenide.page(AddOrders.class);

        addOrders.click(addOrders.getManualTab());
//        addOrders.fillCustomer(inputs.getCustNameInput(),
//                inputs.getCustAddressInput(),
//                "John",
//                "Leipziger Straße 1, 14473",
//                "1",
//                "Leipziger Straße 1, 14473 Potsdam, Germany");

//        addOrders.checkCustomer("John", "Leipziger Straße 1, 14473",
//                "Leipziger Straße 1, 14473 Potsdam, Germany");
        log.info("Added customer 1");

        addOrders.scrollToEl(addOrders.getAddOrdersHeader());
        inputs.getCustNameInput().click();
//        addOrders.fillCustomer(inputs.getCustNameInput(),
//                inputs.getCustAddressInput(),
//                "Ashly",
//                "Marienwerderstraße 1, 16244 Schorfheide",
//                " !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~",
//                "Marienwerderstraße 1, 16244 Schorfheide, Germany");
        log.info("Added customer 2");

        addOrders.scrollToEl(addOrders.getAddOrdersHeader());
        inputs.getCustNameInput().click();
//        addOrders.fillCustomer(inputs.getCustNameInput(),
//                inputs.getCustAddressInput(),
//                "Tariel",
//                "Alexanderplatz 5, 10178 Berlin",
//                "+234234234234234234123123",
//                "Alexanderplatz 5, 10178 Berlin, Germany");
        log.info("Added customer 3");

        addOrders.click(addOrders.getPlanTour());
        ToursOverview toursOverview = Selenide.page(ToursOverview.class);

        toursOverview.getAssignedOrders().shouldHave(text("3 of 3"));
        toursOverview.getVehiclesUsed().shouldHave(text("2 of 2"));
        toursOverview.click(toursOverview.getButtonDownloadAssigned());
        Selenide.sleep(10000);

        List<String[]> tourDetails = ExcelReader.readAsStrings(Browser.checkDownloadedCsv(), 1, ',');
        toursOverview.checkDataInExcel("Tariel", tourDetails.get(0));
        toursOverview.checkDataInExcel("Alexanderplatz 5, 10178 Berlin, Germany", tourDetails.get(0));

        toursOverview.checkDataInExcel("John", tourDetails.get(1));
        toursOverview.checkDataInExcel("Leipziger Straße 1, 14473 Potsdam, Germany", tourDetails.get(1));

        toursOverview.checkDataInExcel("Ashly", tourDetails.get(2));
        toursOverview.checkDataInExcel("Marienwerderstraße 1, 16244 Schorfheide, Germany", tourDetails.get(2));
        log.info("CSV file with orders was checked");

        toursOverview.click(toursOverview.getButtonViewTour().get(0));
        toursOverview.checkAdress("1", "Alexanderplatz 5, 10178 Berlin, Germany");
        toursOverview.checkAdress("2", "Leipziger Straße 1, 14473 Potsdam, Germany");

        toursOverview.checkButtons();
        toursOverview.checkTripData("2","2");
        toursOverview.checkMapPoints();
        toursOverview.checkGetDirections();
        toursOverview.getFirstPoint().shouldBe(visible);
        toursOverview.getSecondPoint().shouldBe(visible);

        toursOverview.click(toursOverview.getButtonNextTour());

        toursOverview.checkAdress("1", "Marienwerderstraße 1, 16244 Schorfheide, Germany");
        toursOverview.checkButtons();
        toursOverview.checkTripData("1","1");
        toursOverview.checkMapPoints();
        toursOverview.checkGetDirections();
        toursOverview.getFirstPoint().shouldBe(visible);
        log.info("Checked all paths");
    }
}
