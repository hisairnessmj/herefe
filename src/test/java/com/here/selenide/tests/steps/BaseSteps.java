package com.here.selenide.tests.steps;

import com.codeborne.selenide.SelenideElement;
import com.here.selenide.webpages.GreetingFleetAndDepot;
import com.here.selenide.webpages.ToursOverview;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

import java.util.stream.Stream;

import static com.codeborne.selenide.Condition.visible;

public class BaseSteps {

    @Step("Skipping Intro")
    public static void skipIntro(GreetingFleetAndDepot greetAndDepot) {
        greetAndDepot.getNextButton().click();
        greetAndDepot.getNextButton().click();
        greetAndDepot.getStartPlanningButton().click();
        greetAndDepot.getAccCookiesBut().click();
    }

    @Step("Check visibility of tour with three points on map")
    public static void checkVisibilityTour(ToursOverview toursOverview) {
        Stream<SelenideElement> s = Stream.of(toursOverview.getTourDetails(),
                toursOverview.getFirstPoint(),
                toursOverview.getSecondPoint(),
                toursOverview.getThirdPoint());
        s.forEach(e -> e.shouldBe(visible));
    }
}
