package com.here.selenide.tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.conditions.Attribute;
import com.here.selenide.listener.TestAllureListener;
import com.here.selenide.tests.steps.BaseSteps;
import com.here.selenide.webpages.AddOrders;
import com.here.selenide.webpages.GreetingFleetAndDepot;
import com.here.selenide.webpages.Inputs;
import com.here.selenide.webpages.ToursOverview;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static org.apache.commons.lang3.RandomUtils.nextInt;
@Log4j2
@Listeners({TestAllureListener.class})
public class PathNegative extends Hooks{

    @Test
    public void pathNegative() throws InterruptedException {
        GreetingFleetAndDepot greetAndDepot = Selenide.page(GreetingFleetAndDepot.class);
        Inputs inputs = new Inputs();
        inputs.init();
        log.info(String.format("Opened page: %s", Selenide.title()));

        BaseSteps.skipIntro(greetAndDepot);

        greetAndDepot.getFleetAndDepot().shouldBe(Condition.visible);
        greetAndDepot.click(greetAndDepot.getCompanyName());
        inputs.getCompanyNameInput().sendKeys("someCompany");
        greetAndDepot.printByChar(inputs.getCompanyAddressInput(), "Nalepastraße 5, 12459 Berlin");

        greetAndDepot.getAddressVariants().findBy(text("Nalepastraße 5, 12459 Berlin, Germany")).click();
        Assert.assertEquals(inputs.getCompanyAddressInput().getValue(), "Nalepastraße 5, 12459 Berlin, Germany");
        greetAndDepot.click(greetAndDepot.getNextToCustomerButton());
        log.info("Entered starting address");

        AddOrders addOrders = Selenide.page(AddOrders.class);

        addOrders.click(addOrders.getManualTab());
        inputs.getCustNameInput().click();
        addOrders.fillCustomerNegative(inputs.getCustNameInput(),
                inputs.getCustAddressInput(),"1","as1d2fgh1jkl3;");
        addOrders.getNoSuchAddress().shouldBe(visible);

        addOrders.scrollToEl(addOrders.getPlus());
        addOrders.getPlus().shouldHave(Attribute.attribute("disabled"));
        addOrders.getNoOrdersEntered().shouldBe(visible);

        addOrders.scrollToEl(addOrders.getAddOrdersHeader());
        inputs.getCustAddressInput().click();
        inputs.getCustAddressInput().clear();
        addOrders.getNoSuchAddress().shouldNotBe(visible);

        inputs.getCustAddressInput().sendKeys("Al exander" + Keys.ENTER);
        addOrders.getNoSuchAddress().shouldBe(visible);
        Selenide.sleep(1000);

        inputs.getCustAddressInput().clear();
        addOrders.getNoSuchAddress().shouldNotBe(visible);

        inputs.getCustAddressInput().sendKeys(String.valueOf(nextInt(10, 30)) + Keys.ENTER);
        addOrders.getNoSuchAddress().shouldBe(visible);
        Selenide.sleep(1000);

        inputs.getCustAddressInput().clear();
        addOrders.getNoSuchAddress().shouldNotBe(visible);

        inputs.getCustAddressInput().sendKeys("Alexander 7" + Keys.ENTER);
        addOrders.getNoSuchAddress().shouldBe(visible);

        inputs.getCustAddressInput().clear();
        addOrders.getNoSuchAddress().shouldNotBe(visible);

        inputs.getCustAddressInput().sendKeys(" !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~" + Keys.ENTER);
        addOrders.getNoSuchAddress().shouldBe(visible);

        inputs.getCustAddressInput().clear();
        addOrders.getNoSuchAddress().shouldNotBe(visible);
        log.info("Entered invalid address");

//        addOrders.fillCustomer(inputs.getCustNameInput(),
//                inputs.getCustAddressInput(),"Alex","Togliatti (Tol'yatti), Volga Federal District", "1",
//                "Togliatti (Tol'yatti), Volga Federal District, Russia");
        addOrders.click(addOrders.getPlanTour());
        log.info("Entered customer from Russia");

        ToursOverview toursOverview = Selenide.page(ToursOverview.class);

        toursOverview.getNoPossiblePlanning().shouldBe(visible);

        toursOverview.click(toursOverview.getPrevious());
        addOrders.click(addOrders.getRemoveOrders());

//        addOrders.fillCustomer(inputs.getCustNameInput(),
//                inputs.getCustAddressInput(),"Scriptalert","Aaaa Scriptalert(1)", "_",
//                "A.A.A.A.Aaa Abacus");
        addOrders.click(addOrders.getPlanTour());
        log.info("Entered customer with impossible address");
        Selenide.sleep(3000);
    }
}
