package com.here.selenide.tests;

import com.here.selenide.webdriver.Browser;
import com.here.selenide.webdriver.Log;
import lombok.extern.log4j.Log4j2;
import net.lightbody.bmp.core.har.HarEntry;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.open;

@Log4j2
public class Hooks {

    @BeforeMethod
    public void setUp() {
        String prop = System.getProperty("profileId");
        if (prop.equals("local-testing")) {
            Browser.loadDriver();
        } else if (prop.equals("remote-testing")) {
            Browser.loadRemoteDriver();
        }

        Log.decorateClick();
        log.info("Starting browser");
        open("https://wegodeliver.here.com/");

        assertNoErrorCodes();
        checkImagesDownloadTimings();
    }

    private void assertNoErrorCodes() {
        List<HarEntry> harEntries = Browser.getProxyServer().getHar().getLog().getEntries();
        boolean areThereErrorCodes = harEntries.stream().anyMatch(r
                -> r.getResponse().getStatus() > 400
                && r.getResponse().getStatus() < 599);
        Assert.assertFalse(areThereErrorCodes);
    }

    public void checkImagesDownloadTimings() {
        List<HarEntry> harEntries = Browser.getProxyServer().getHar().getLog().getEntries();

        List<Long> responceTimings = harEntries.stream()
                .filter(r -> r.getRequest().getUrl().contains("https://4.traffic.maps.ls.hereapi.com/maptile"))
                .map(HarEntry::getTime).collect(Collectors.toList());

        log.info(String.format("max timing for map tile: %d", Collections.max(responceTimings)));
        log.info(String.format("min timing for map tile: %d", Collections.min(responceTimings)));
        double avgTimings = responceTimings.stream()
                .mapToDouble(a -> a)
                .average().getAsDouble();
        log.info(String.format("avg timing for map tile: %,.2f", avgTimings));

        boolean timingsInterval = harEntries.stream()
                .filter(r -> r.getRequest().getUrl().contains("https://4.traffic.maps.ls.hereapi.com/maptile"))
                .allMatch(r -> r.getTime() < 1500);
        if (!timingsInterval) log.warn("Timings for map tiles exceed limit of 1500 ms!");
    }

    @AfterMethod
    public void tearDown() {
        Browser.closeBrowser();
        log.info("Closed browser");
        Browser.cleanDownloads();
    }
}
