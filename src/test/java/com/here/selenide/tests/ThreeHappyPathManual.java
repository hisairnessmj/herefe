package com.here.selenide.tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.here.selenide.entity.User;
import com.here.selenide.listener.TestAllureListener;
import com.here.selenide.tests.steps.BaseSteps;
import com.here.selenide.utils.PDFReader;
import com.here.selenide.webdriver.Browser;
import com.here.selenide.webpages.AddOrders;
import com.here.selenide.webpages.GreetingFleetAndDepot;
import com.here.selenide.webpages.Inputs;
import com.here.selenide.webpages.ToursOverview;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.codeborne.selenide.Condition.*;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

@Log4j2
@Listeners({TestAllureListener.class})
public class ThreeHappyPathManual extends Hooks {

    public List<User> getData() {
        return Stream
                .of(
                        new User("John", "Alexanderplatz 7, 10178", "1",
                                "Alexanderplatz 7, 10178 Berlin, Germany"),
                        new User("Ashly", "Frankfurter Allee 102, 10247", "+49 30 901820",
                                "Frankfurter Allee 102, 10247 Berlin, Germany"),
                        new User("Semen", "Rosenfelder Ring 11, 10315 Berlin", "+49_30_11",
                                "Rosenfelder Ring 11, 10315 Berlin, Germany")
                )
                .collect(Collectors.toList());
    }

    @Test
    public void threeHappyPathManual() throws InterruptedException {
        List<User> users = getData();

        GreetingFleetAndDepot greetAndDepot = Selenide.page(GreetingFleetAndDepot.class);
        Inputs inputs = new Inputs();
        inputs.init();
        log.info(String.format("Opened page: %s", greetAndDepot.getPageName()));

        BaseSteps.skipIntro(greetAndDepot);
        log.info("Skipped introduction");

        greetAndDepot.getFleetAndDepot().shouldBe(Condition.visible);
        greetAndDepot.click(greetAndDepot.getCompanyName());
        inputs.getCompanyNameInput().sendKeys("someCompany");
        greetAndDepot.printByChar(inputs.getCompanyAddressInput(), "Nalepastraße 5, 12459 Berlin")
                .getAddressVariants().findBy(text("Nalepastraße 5, 12459 Berlin, Germany"))
                .click();

        Assert.assertEquals(inputs.getCompanyAddressInput().getValue(), "Nalepastraße 5, 12459 Berlin, Germany");
        greetAndDepot.click(greetAndDepot.getNextToCustomerButton());

        AddOrders addOrders = Selenide.page(AddOrders.class);
        log.info(String.format("Opened page: %s", addOrders.getPageName()));

        addOrders.click(addOrders.getManualTab()).fillCustomer(inputs.getCustNameInput(),
                inputs.getCustAddressInput(), users.get(0));

        addOrders.scrollToEl(addOrders.getPlus()).getGridWithCust()
                .shouldBe(visible).find(By.xpath(".//div[@id='label-order-name-0']"))
                .shouldHave(exactText(users.get(0).getName()));
        addOrders.getGridWithCust().shouldBe(visible).find(By.xpath(".//div[@id='label-order-address-0']"))
                .shouldHave(exactText(users.get(0).getFullAddress()));
        log.info("Added customer 1");

        addOrders.scrollToEl(addOrders.getAddOrdersHeader());
        inputs.getCustNameInput().click();
        addOrders.fillCustomer(inputs.getCustNameInput(),
                inputs.getCustAddressInput(),
                users.get(1));

        addOrders.scrollToEl(addOrders.getPlus()).checkCustomer(users.get(1));
        log.info("Added customer 2");

        addOrders.scrollToEl(addOrders.getAddOrdersHeader());
        inputs.getCustNameInput().click();
        addOrders.fillCustomer(inputs.getCustNameInput(),
                inputs.getCustAddressInput(),
                users.get(2));

        addOrders.scrollToEl(addOrders.getPlus()).checkCustomer(users.get(2));
        log.info("Added customer 3");

        addOrders.click(addOrders.getPlanTour());

        ToursOverview toursOverview = Selenide.page(ToursOverview.class);
        log.info(String.format("Opened page: %s", toursOverview.getPageName()));

        BaseSteps.checkVisibilityTour(toursOverview);

        toursOverview.checkAdress("1", users.get(2).getFullAddress())
                .checkAdress("2", users.get(1).getFullAddress())
                .checkAdress("3", users.get(0).getFullAddress());
        log.info("Checked addresses");

        toursOverview.checkButtons().checkTripData("3", "3")
                .checkMapPoints().checkGetDirections().click(toursOverview.getDownloadPdf());
        Selenide.sleep(15000);

        String pdfContent = PDFReader.getPdfContent(Browser.checkDownloadedPdf());

        assertNotNull(pdfContent);
        assertFalse(pdfContent.isEmpty());
        Assert.assertTrue(pdfContent.contains("STOPS"));
        Assert.assertTrue(pdfContent.contains("DELIVERIES"));
        Assert.assertTrue(pdfContent.contains("DURATION"));

        Assert.assertTrue(pdfContent.contains(users.get(2).getFullAddress()));
        Assert.assertTrue(pdfContent.contains(users.get(1).getFullAddress()));
        Assert.assertTrue(pdfContent.contains(users.get(0).getFullAddress()));

        Assert.assertTrue(pdfContent.contains("+49 30 901820"));
        Assert.assertTrue(pdfContent.contains("Semen"));
        log.info("Checked outgoing pdf with tour");
    }
}
