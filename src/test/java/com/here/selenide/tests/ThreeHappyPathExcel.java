package com.here.selenide.tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.here.selenide.listener.TestAllureListener;
import com.here.selenide.tests.steps.BaseSteps;
import com.here.selenide.utils.ExcelReader;
import com.here.selenide.webdriver.Browser;
import com.here.selenide.webpages.AddOrders;
import com.here.selenide.webpages.GreetingFleetAndDepot;
import com.here.selenide.webpages.Inputs;
import com.here.selenide.webpages.ToursOverview;
import com.opencsv.exceptions.CsvException;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.codeborne.selenide.Condition.text;
@Log4j2
@Listeners({TestAllureListener.class})
public class ThreeHappyPathExcel extends Hooks{

    @Test
    public void threeHappyPathExcel() throws IOException, URISyntaxException, CsvException, InterruptedException {
        GreetingFleetAndDepot greetAndDepot = Selenide.page(GreetingFleetAndDepot.class);
        Inputs inputs = new Inputs();
        inputs.init();
        log.info(String.format("Opened page: %s", Selenide.title()));

        BaseSteps.skipIntro(greetAndDepot);

        greetAndDepot.getFleetAndDepot().shouldBe(Condition.visible);
        greetAndDepot.click(greetAndDepot.getCompanyName());
        inputs.getCompanyNameInput().sendKeys("someCompany");
        inputs.getCompanyAddressInput().click();
        greetAndDepot.printByChar(inputs.getCompanyAddressInput(), "Nalepastraße");
        inputs.getCompanyAddressInput().sendKeys(Keys.ENTER);
        greetAndDepot.getAddressVariants().findBy(text("Nalepastraße, 12459 Berlin, Germany")).click();

        Assert.assertEquals(inputs.getCompanyAddressInput().getValue(),"Nalepastraße, 12459 Berlin, Germany");
        greetAndDepot.click(greetAndDepot.getNextToCustomerButton());

        AddOrders addOrders = Selenide.page(AddOrders.class);

        addOrders.click(addOrders.getImportTab());
        File file = addOrders.getAddFile().uploadFile(new File("src/test/resources/template.csv"));
        Assert.assertNotNull(file);
        log.info("Uploaded file with customers");

        List<String[]> list = ExcelReader.readAsStrings("src/test/resources/template.csv", 0, ',');
        //addOrders.checkColumns(list.get(0));
        //addOrders.checkCategoryOptions(new String[] {"Unassigned", "Address", "ID", "Demand", "Phone",
        //"e-mail", "Name", "Notes", "Latitude", "Longitude", "Service Time (min)", "Start Time", "End Time"});

        addOrders.click(addOrders.getSaveInput());
//        addOrders.checkCategories();
//        addOrders.checkInputValues("#1",
//                "Ida Redwoods",
//                "611234567",
//                "",
//                "Tabbertstraße 14, 12459 Berlin, Germany",
//                "Please pick up the cooler bags",
//                "8:00",
//                "13:00",
//                "10",
//                "3");
        addOrders.click(addOrders.getConfirmInput());

        addOrders.scrollToEl(addOrders.getGridWithCust());
//        addOrders.checkCustomer("Ida Redwoods", "Tabbertstraße 14",
//                "Tabbertstraße 14, 12459 Berlin, Germany");
//
//        addOrders.checkCustomer("Thierry Davies", "Mentelinstraße 30",
//                "Mentelinstraße 30, 12459 Berlin, Germany");
//
//        addOrders.checkCustomer("Lennie Travers", "Baumschulenstraße 1A",
//                "Baumschulenstraße 1A, 12437 Berlin, Germany");
        log.info("Checked uploaded values of customers");

        addOrders.click(addOrders.getPlanTour());

        ToursOverview toursOverview = Selenide.page(ToursOverview.class);

        BaseSteps.checkVisibilityTour(toursOverview);

        toursOverview.checkAdress("1", "Mentelinstraße 30, 12459 Berlin, Germany");
        toursOverview.checkAdress("2", "Tabbertstraße 14, 12459 Berlin, Germany");

        toursOverview.scrollToEl(toursOverview.getThirdPoint());
        toursOverview.checkAdress("3", "Baumschulenstraße 1A, 12437 Berlin, Germany");
        log.info("Checked addresses");

        toursOverview.checkButtons();
        toursOverview.checkTripData("3","8");
        toursOverview.checkMapPoints();
        toursOverview.checkGetDirections();

        toursOverview.click(toursOverview.getDownloadCsv());
        Selenide.sleep(10000);

        List<String[]> tourDetails = ExcelReader.readAsStrings(Browser.checkDownloadedCsv(), 1, ';');
        toursOverview.checkDataInExcel("Thierry Davies", tourDetails.get(0));
        toursOverview.checkDataInExcel("Mentelinstraße 30, 12459 Berlin, Germany", tourDetails.get(0));
        toursOverview.checkDataInExcel("6112345678", tourDetails.get(0));

        toursOverview.checkDataInExcel("Ida Redwoods", tourDetails.get(1));
        toursOverview.checkDataInExcel("2", tourDetails.get(1));
        toursOverview.checkDataInExcel("Tabbertstraße 14, 12459 Berlin, Germany", tourDetails.get(1));
        toursOverview.checkDataInExcel("611234567", tourDetails.get(1));

        toursOverview.checkDataInExcel("order_20", tourDetails.get(2));
        toursOverview.checkDataInExcel("Lennie Travers", tourDetails.get(2));
        toursOverview.checkDataInExcel("Baumschulenstraße 1A, 12437 Berlin, Germany", tourDetails.get(2));
        toursOverview.checkDataInExcel("Heavy parcel", tourDetails.get(2));
        log.info("Checked outgoing csv with tour details");
    }
}
